export const isAnonymousUser = () => {
  return (
    JSON.parse(localStorage.getItem("user") || "null")?.data.User.Id === -999
  );
};
