import { createContext, useContext, useState } from "react";
import { logIn } from "./AuthActions";
import { useNavigate, useLocation } from "react-router-dom";

const AuthContext = createContext(null);

export const AuthProvider = ({ children }) => {
  const navigate = useNavigate();
  const location = useLocation();

  const [token, setToken] = useState(
    JSON.parse(localStorage.getItem("user") || "null")?.access_token
  );
  const [errorMessage, setErrorMessage] = useState("");

  const handleLogin = async (formValue) => {
    const userData = await logIn(formValue);

    if ("error" in userData && userData.error) {
      setErrorMessage(userData.errorMessage);
    } else {
      const token = userData.AuthorizationToken.Token;
      localStorage.setItem(
        "user",
        JSON.stringify({ access_token: token, data: userData })
      );
      setToken(token);

      const origin = location.state?.from?.pathname || "/home";
      navigate(origin, { token });
    }
  };

  const handleLogout = () => {
    localStorage.removeItem("user");
    setToken(null);
  };

  const value = {
    token,
    onLogin: handleLogin,
    onLogout: handleLogout,
    errorMessage,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => useContext(AuthContext);
