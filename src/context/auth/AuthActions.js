import { apiUrl } from "../apiUrl";
import { v4 as uuidv4 } from "uuid";

export const logIn = async (formValue) => {
  const { username, password } = formValue || {};

  try {
    const { data } = await apiUrl.post(`Authorization/SignIn`, {
      ...(formValue ? { Username: username, Password: password } : {}),
      Device: {
        PlatformCode: "Web",
        Name: uuidv4(),
      },
    });

    return data;
  } catch (err) {
    return { error: true, errorMessage: err.response.data.Message };
  }
};
