import { apiUrl } from "../apiUrl";
import { isAnonymousUser } from "../../helper";

export const getVideos = async () => {
  const response = await apiUrl.post(`Media/GetMediaList`, {
    MediaListId: 3,
    IncludeCategories: false,
    IncludeImages: true,
    IncludeMedia: false,
    PageNumber: 1,
    PageSize: 15,
  });
  return response.data.Entities;
};

export const getSingleVideo = async (videoId) => {
  try {
    const response = await apiUrl.post(`Media/GetMediaPlayInfo`, {
      MediaId: Number(videoId),
      StreamType: isAnonymousUser ? "TRIAL" : "MAIN",
    });

    return response.data;
  } catch (err) {
    console.log("err", err);
  }
};
