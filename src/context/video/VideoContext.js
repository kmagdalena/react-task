import { createContext, useReducer } from "react";
import videoReducer from "./VideoReducer";

const VideoContext = createContext();

export const VideoProvider = ({ children }) => {
  const initialState = {
    videos: [],
    video: {},
    isLoading: false,
  };

  const [state, dispatch] = useReducer(videoReducer, initialState);

  return (
    <VideoContext.Provider
      value={{
        ...state,
        dispatch,
      }}
    >
      {children}
    </VideoContext.Provider>
  );
};

export default VideoContext;
