import { GET_SINGLE_VIDEO, GET_VIDEOS, SET_LOADING } from "../actionTypes";

const videoReducer = (state, { type, payload }) => {
  switch (type) {
    case GET_VIDEOS:
      return {
        ...state,
        videos: payload,
        isLoading: false,
      };
    case GET_SINGLE_VIDEO:
      return {
        ...state,
        video: payload,
        isLoading: false,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      return state;
  }
};

export default videoReducer;
