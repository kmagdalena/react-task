export const GET_VIDEOS = "GET_VIDEOS";
export const GET_SINGLE_VIDEO = "GET_SINGLE_VIDEO";
export const SET_LOADING = "SET_LOADING";
