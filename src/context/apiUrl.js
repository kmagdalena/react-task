import axios from "axios";

export const apiUrl = axios.create({
  baseURL: "https://thebetter.bsgroup.eu/",
  contentType: "application/json",
});

apiUrl.interceptors.request.use((config) => {
  config.headers["Authorization"] = JSON.parse(
    localStorage.getItem("user") || "null"
  )?.access_token;
  return config;
});
