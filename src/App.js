import "./App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import LogIn from "./components/LogIn";
import Player from "./components/Player";
import Header from "./components/Header";
import ScrollToTop from "./components/ScrollToTop";
import PrivateRoute from "./components/PrivateRoute";
import { AuthProvider } from "./context/auth/AuthContext";
import { VideoProvider } from "./context/video/VideoContext";

function App() {
  return (
    <BrowserRouter>
      <AuthProvider>
        <VideoProvider>
          <ScrollToTop>
            <Header />
            <Routes>
              <Route path="/" element={<LogIn />} />
              <Route path="/home" element={<PrivateRoute />}>
                <Route path="/home" element={<Home />} />
              </Route>
              <Route path="/player/:videoId" element={<PrivateRoute />}>
                <Route path="/player/:videoId" element={<Player />} />
              </Route>
            </Routes>
          </ScrollToTop>
        </VideoProvider>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
