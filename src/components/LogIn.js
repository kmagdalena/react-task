import { useState } from "react";
import { useAuth } from "../context/auth/AuthContext";

function LogIn() {
  const [formValue, setForm] = useState({
    username: "",
    password: "",
  });
  const { onLogin, errorMessage } = useAuth();

  const { username, password } = formValue;

  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm({ ...formValue, [name]: value });
  };

  const submitForm = (e, noLogin = false) => {
    e.preventDefault();

    onLogin(noLogin ? null : { username, password });
  };

  const noLogin = true;

  return (
    <div className="columns is-centered mt-6">
      <form onSubmit={(e) => submitForm(e)} className="box">
        {errorMessage && <p className="help is-danger">{errorMessage}</p>}
        <div className="field">
          <label htmlFor="username" className="label">
            Username:
          </label>
          <input
            type="text"
            onChange={handleChange}
            value={username}
            name="username"
            required
            className={`input ${errorMessage ? "is-danger" : ""}`}
          />
        </div>

        <div className="field">
          <label htmlFor="password" className="label">
            Password:
          </label>
          <input
            type="password"
            onChange={handleChange}
            value={password}
            name="password"
            required
            className={`input ${errorMessage ? "is-danger" : ""}`}
          />
        </div>
        <div className="field">
          <button className="button is-success is-fullwidth" type="submit">
            Log In
          </button>
        </div>
        <div className="field">
          <button
            className="button is-warning is-fullwidth"
            onClick={(e) => submitForm(e, noLogin)}
          >
            Enter without login
          </button>
        </div>
      </form>
    </div>
  );
}

export default LogIn;
