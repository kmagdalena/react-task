import { useEffect, useContext } from "react";
import VideoContext from "../context/video/VideoContext";
import { getVideos } from "../context/video/VideoActions";
import { GET_VIDEOS, SET_LOADING } from "../context/actionTypes.js";
import { Link } from "react-router-dom";
import Spinner from "./Spinner/Spinner.js";

function Home() {
  const { videos, isLoading, dispatch } = useContext(VideoContext);

  useEffect(() => {
    const fetchVideos = async () => {
      dispatch({ type: SET_LOADING });
      const videos = await getVideos();
      dispatch({ type: GET_VIDEOS, payload: videos });
    };

    fetchVideos();
  }, [dispatch]);

  const getImage = (images) => {
    const frameImage = images.find(
      (image) => image.ImageTypeCode === "FRAME"
    )?.Url;
    return frameImage || "/movie-placeholder.png";
  };

  if (isLoading) {
    return <Spinner />;
  } else {
    return (
      <div className="columns is-flex-wrap-wrap p-2">
        {videos.map((video) => {
          return (
            <div key={video.Id} className="column is-one-third">
              <Link to={`/player/${video.Id}`}>
                <p className="is-uppercase	has-text-weight-bold">
                  {video.Title}
                </p>
                <div className="img-container">
                  <img src={getImage(video.Images)} alt="frame"></img>
                </div>
              </Link>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Home;
