import "./Spinner.scss";

function Spinner() {
  return (
    <div className="is-flex is-justify-content-center">
      <span className="loader"></span>
    </div>
  );
}

export default Spinner;
