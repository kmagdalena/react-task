import { useNavigate, useLocation } from "react-router-dom";
import { useAuth } from "../context/auth/AuthContext";
import { isAnonymousUser } from "../helper";
import { Link } from "react-router-dom";

function Header() {
  const navigate = useNavigate();
  const location = useLocation();
  const { token, onLogout } = useAuth();

  const logOut = () => {
    onLogout();
    navigate("/");
  };
  const showLogOutButton = token && !isAnonymousUser();
  const isMainPage = location.pathname === "/";

  return (
    <nav
      className="navbar is-primary is-flex is-align-items-center is-justify-content-center mb-3 px-6"
      role="navigation"
      aria-label="main navigation"
    >
      <h1 className="has-text-weight-bold navbar-start">VIDEO APP</h1>
      {showLogOutButton ? (
        <button className="navbar-end button is-link" onClick={logOut}>
          Logout
        </button>
      ) : !isMainPage ? (
        <Link to={`/`}>
          <button className="navbar-end button is-link">Login</button>
        </Link>
      ) : null}
    </nav>
  );
}

export default Header;
