import { useParams } from "react-router-dom";
import { useEffect, useContext } from "react";
import VideoContext from "../context/video/VideoContext";
import { getSingleVideo } from "../context/video/VideoActions";
import { GET_SINGLE_VIDEO, SET_LOADING } from "../context/actionTypes.js";
import ReactPlayer from "react-player/lazy";
import Spinner from "./Spinner/Spinner.js";

function Player() {
  const { video, dispatch, isLoading } = useContext(VideoContext);

  const params = useParams();

  useEffect(() => {
    const getVideo = async () => {
      dispatch({ type: SET_LOADING });
      const videoData = await getSingleVideo(params.videoId);
      dispatch({ type: GET_SINGLE_VIDEO, payload: videoData });
    };

    getVideo();
  }, [dispatch, params.videoId]);
  const { ContentUrl, Title } = video;
  const sampleVideoUrl = "https://www.youtube.com/embed/a3ICNMQW7Ok";

  if (isLoading) {
    return <Spinner />;
  } else {
    return (
      <div className="is-flex is-flex-direction-column is-align-items-center">
        <h2 className="is-uppercase	has-text-weight-bold">{Title}</h2>
        <ReactPlayer
          url={ContentUrl || sampleVideoUrl}
          controls={true}
          playing={true}
        />
      </div>
    );
  }
}

export default Player;
